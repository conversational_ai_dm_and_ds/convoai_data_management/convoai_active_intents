import os 
from pathlib import Path
import yaml
import glob
import traceback 
import re 
import base64
import datetime
import pandas as pd 
import json 
space_trailing = '         '
path = Path(__file__).parent
intent_list = {}
path = os.path.abspath(os.path.join(path, os.pardir))

def detect_data_changes(yml_data, nucleus_data, load_date, key_columns): 
    """
    yml_data: yml file data
    nucleus_data: nucleus table data

    yml vs nucleus: 
    if yml has the data, but nucleus not, new data is added in yml
    if yml does not have the data, but nulceus has the data, the data is removed in rasa code. 
    """
    train_data = yml_data.copy()
    prev_data = nucleus_data.copy()
    train_data['source_nlu'] = 'rasa'
    prev_data['source_db'] = 'snowflake' 
    train_data['STATUS'] = True # set all data from yml file as True, meaning the data are all active. 
    df = pd.merge(train_data,prev_data,on=key_columns+['STATUS'],how='outer')  

    # detect the changes 
    change_trigger = False
    # new intent-utterances (nlu file has the data, but snowflake db does not)
    if(len(df[df.source_db.isnull()])>0): 
        df.loc[df.source_db.isnull(), 'STATUS'] = True
        df.loc[df.source_db.isnull(), 'START_DATE'] = str(load_date)
        df.loc[df.source_db.isnull(), 'END_DATE'] = '9999-01-01' 
        change_trigger = True 
        print(f"{len(df.loc[df.source_db.isnull()])} records are newly added in training.") 

    # removed intent-utterances (nlu file does not have the data, but snowflake db have it)
    if(len(df[df.source_nlu.isnull()])>0): 
        df.loc[df.source_nlu.isnull(), 'STATUS'] = False
        df.loc[df.source_nlu.isnull(), 'END_DATE'] = str(load_date)
        change_trigger = True 
        print(f"{len(df.loc[df.source_nlu.isnull()])} records are newly removed in training.") #
    df = df.drop(columns=['source_db', 'source_nlu'])
    return change_trigger, df 

def update_data_changes_general(nucleus_data, yml_data, table_name, snow, load_date, key_columns): 
    nucleus_data = nucleus_data.drop_duplicates() # remove duplicates if there are.
    change_trigger = False  # initialization  
    output_df = pd.DataFrame()
    # df.loc[df.INTENT == 'debit_card_general', 'START_DATE'] = str('2022-11-30') # one time reset the start date
    if (len(nucleus_data)==0): # initial settings for intent start date and end date. 
        print(space_trailing + f"INFO: Currently snowflake table: {table_name} has no data.")
        yml_data['STATUS'] = bool(1)
        yml_data['START_DATE'] ='2020-01-06'
        yml_data['END_DATE'] = '9999-01-01'
        yml_data['LOAD_DATE'] = str(load_date)
        if(len(yml_data)>0): 
            snow.write_data(yml_data, table_name)
            output_df = yml_data
            print(space_trailing+f"INFO: YMAL data is first loaded into {table_name}. Totally {len(yml_data)} rows are written to Snowflake Table.")
    else: 
        prev_data = nucleus_data
        change_trigger, output_df = detect_data_changes(yml_data, prev_data, load_date, key_columns) ## detect the changes  

        # if change is detected, it will write the changes to snowflake. 
        if(change_trigger): 
            print(space_trailing + f"INFO: Changes are detected in {table_name} on {load_date}.")

            output_df = output_df[ key_columns + ['STATUS', 'START_DATE', 'END_DATE','LOAD_DATE']]
            output_df = output_df.drop_duplicates()
            if(len(output_df)>0):
                output_df['LOAD_DATE'] = output_df.LOAD_DATE.apply(lambda x: str(x).split()[0])
                output_df['START_DATE'] = output_df.START_DATE.apply(lambda x: str(x).split()[0])
                output_df['END_DATE'] = output_df.END_DATE.apply(lambda x: str(x).split()[0])
                snow.write_data(output_df, table_name)
                print(space_trailing+f"INFO: {table_name} is updated due to yaml data changes.")
                # write_master_utterance_to_snowflake(snow, output_df, table_name=deposit_table_name_master_utter, create_table=False)
        else: 
            print(space_trailing + f"INFO: No changes are detected in {table_name} on {load_date}.")
    return output_df

def get_file_content(project, file_path, branch: str='main'): 
    f = project.files.get(file_path=file_path, ref=branch)

    file_content = base64.b64decode(f.content).decode("utf-8")
    # file_content = file_content.replace('\\n', '\n')
    data_dict = yaml.safe_load(file_content)
    return data_dict 
  
def get_all_yml_files(project, folder_path, branch: str='main'): 
    items = project.repository_tree(path=folder_path, ref='main')
    file_list = []
    data_list = []
    for x in items: 
        table_name = x['name']
        if('.yml' in table_name): 
            file_path = folder_path+'/'+table_name
            file_list.append(file_path)
            data = get_file_content(project, file_path,branch) 
            data_list.append(data)
    return file_list, data_list

########################master utterance data extraction and update###############################################################################################################################
def extract_utterance(utter): 
    if utter:
        result = re.sub(r'\{.*?\}', '', utter[1:].strip()) 
        result_2 = re.sub(r'\(.*?\)', '', result)
        result_3 = result_2.replace('[','')
        result_4 = result_3.replace(']','')
        return result_4 
    else: 
        return '' 
def extract_intent_entities(intent, utter, intent_list, org_text_list, text_list, ent_list, value_list, actual_value_list, start_list, end_list): 
    if utter:
        clean_utter = extract_utterance(utter)
        utter_new = utter[1:].strip()
        value_ent0 = re.findall("\[[^\]}),]*\]\{.*?\}", utter_new)
        if(len(value_ent0)>=1):
            for i in range(len(value_ent0)): 
                start = utter_new.find(value_ent0[i])
                actual_val = re.findall("\[.*?\]", value_ent0[i])[0]
                actual_val = val = re.sub(r"[\[\]]",'',str(actual_val))
                ent = re.findall("\{.*?\}", value_ent0[i])[0]
                # print(actual_val, ent)
                en = json.loads(ent)
                intent_list.append(intent)
                org_text_list.append(utter_new)
                text_list.append(clean_utter)
                ent_list.append(str(en['entity'])) 
                value_list.append(str(en['value']))
                actual_value_list.append(actual_val)
                start_list.append(start)
                end_list.append(start+len(actual_val)-1)
        value_ent = re.findall(r"\[[^\]}),]*\]\(.*?\)", utter_new)
        if(len(value_ent)>0): 
            for i in range(len(value_ent)): 
                val = re.findall("\[.*?\]", value_ent[i])[0]
                en = re.findall("\(.*?\)", value_ent[i])[0]
                val = re.sub(r"[\[\]]",'',str(val))
                en = re.sub(r"[\(\)]",'',str(en))
                intent_list.append(intent)
                org_text_list.append(utter_new)
                text_list.append(clean_utter)
                ent_list.append(str(en)) 
                value_list.append(str(val))
                actual_value_list.append(str(val))
                start = utter_new.find(value_ent[i])
                start_list.append(start)
                end_list.append(start+len(val)-1)                    
        if((len(value_ent0)<1) & (len(value_ent)<1)): 
            intent_list.append(intent)
            org_text_list.append(utter_new)
            text_list.append(clean_utter)
            ent_list.append('') 
            value_list.append('')
            actual_value_list.append('')
            start_list.append(0)
            end_list.append(0)
    return intent_list, org_text_list, text_list, ent_list, value_list, actual_value_list, start_list, end_list

def get_intent_utterances(data_list):
    intent_list = {}
    for nlu_intents in data_list: 
        for item in nlu_intents['nlu']:
            if 'intent' in item.keys():
                text = item['examples']
                utterances = text.split("\n")  
                utter_list = []
                for utter in utterances:
                    if utter:
                        result = extract_utterance(utter)
                        utter_list.append(result)
                intent_list[item['intent']] = utter_list
    return intent_list

def extract_all_training_data(data_list): 
    text_list = []
    intent_list = []
    ent_list = [] 
    value_list = [] 
    actual_value_list = [] 
    start_list = [] 
    end_list = [] 
    org_text_list = []
    for nlu_intents in data_list: 
        for item in nlu_intents['nlu']:
            if 'intent' in item.keys():
                text = item['examples']
                utterances = text.split("\n")  
                utter_list = []
                for utter in utterances:
                    if utter:
                        intent_list, org_text_list, text_list, ent_list, value_list, actual_value_list, start_list, end_list = extract_intent_entities(item['intent'], utter, intent_list, org_text_list, text_list, ent_list, value_list, actual_value_list, start_list, end_list)
    df = pd.DataFrame(list(zip(intent_list, org_text_list, text_list, ent_list, value_list, actual_value_list, start_list, end_list)), columns = ['intent','original','utterance','entity','value', 'actual value', 'start', 'end'])
    return df
########################end of master utterance data update###############################################################################################################################

########################synonym data extraction and update###############################################################################################################################
def get_synonyms(data_list): 
    synonyms_list = {}
    for items in data_list: 
        for item in items['nlu']: 
            if 'synonym' in item.keys(): 
                text = item['examples']
                text = text.replace("- ", "")
                keywords = text.split("\n")  
                synonyms_list[item['synonym']] = keywords
    if (len(synonyms_list)> 0): 
        synonyms_data = pd.concat({k: pd.Series(v) for k, v in synonyms_list.items()})
        synonyms_data = synonyms_data.to_frame().reset_index()
        synonyms_data.columns = ['SYNONYM', 'INDEX', 'WORD']
        synonyms_data = synonyms_data.drop(columns=['INDEX'])
        synonyms_data = synonyms_data.fillna("")
        synonyms_data = synonyms_data[synonyms_data.WORD!=""]
    else: 
        synonyms_data = pd.DataFrame()
        print(f"No synonym data is setup for the bot.")
    return synonyms_data  
########################end of synonym data extraction and update###############################################################################################################################


########################lookup data extraction and update###############################################################################################################################
def get_all_lookups(data_list): 
    # lookup_list = {}
    final_df = pd.DataFrame()
    for items in data_list: 
        for item in items['nlu']: 
            if 'lookup' in item.keys(): 
                text = item['examples']
                if (item['lookup'] == 'escalation_words') & ('Category:' in text): # added category into escalation word list
                    keywords = re.findall(r"(?<=word: )(.*?)(?=\n)", text)
                    categories = re.findall(r"(?<=Category: )(.*?)(?=\n)", text)
                    if(len(keywords)!= len(categories)): 
                        print("Warning: The escalation words in rasa have error in word and category.")
                    df = pd.DataFrame(list(zip(keywords, categories)), columns =['WORD', 'CATEGORY'])
                else: 
                    text = text.replace("- ", "")
                    keywords = text.split("\n")  
                    categories = []
                    df = pd.DataFrame(keywords, columns =['WORD'])
                    df['CATEGORY'] = ''
                df['LOOKUP'] = item['lookup']
                final_df = pd.concat([final_df, df], ignore_index=True, axis=0)
    final_df = final_df.fillna("")
    final_df = final_df[final_df.WORD!=""]
    return final_df[['LOOKUP', "WORD", "CATEGORY"]] 

########################end of lookup data extraction and update###############################################################################################################################
